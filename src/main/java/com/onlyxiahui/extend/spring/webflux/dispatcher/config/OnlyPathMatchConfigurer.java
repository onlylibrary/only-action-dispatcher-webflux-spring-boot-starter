
package com.onlyxiahui.extend.spring.webflux.dispatcher.config;

import org.springframework.web.reactive.config.PathMatchConfigurer;

/**
 * Description <br>
 * Date 2020-01-13 23:54:16<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public class OnlyPathMatchConfigurer extends PathMatchConfigurer {

}
