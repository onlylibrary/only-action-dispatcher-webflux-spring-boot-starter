package com.onlyxiahui.extend.spring.webflux.dispatcher.result.handler;

import org.springframework.web.reactive.HandlerResult;
import org.springframework.web.server.ServerWebExchange;

/**
 * Description <br>
 * Date 2020-06-08 14:46:00<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */

public interface ResponseBodyHandler {

	/**
	 * 
	 * 处理响应数据<br>
	 * Date 2020-06-11 16:49:49<br>
	 * 
	 * @param exchange
	 * @param result
	 * @return
	 * @since 1.0.0
	 */
	public Object handle(ServerWebExchange exchange, HandlerResult result);
}
