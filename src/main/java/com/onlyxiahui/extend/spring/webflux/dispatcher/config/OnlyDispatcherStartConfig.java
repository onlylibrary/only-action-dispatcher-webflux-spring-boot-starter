package com.onlyxiahui.extend.spring.webflux.dispatcher.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.reactive.accept.RequestedContentTypeResolver;
import org.springframework.web.reactive.config.PathMatchConfigurer;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;

import com.onlyxiahui.extend.action.spring.boot.config.OnlyDispatcherConfig;
import com.onlyxiahui.extend.spring.webflux.dispatcher.result.OnlyResponseBodyHandleConfig;

/**
 * <br>
 * Date 2019-12-03 17:32:35<br>
 * 
 * @author XiaHui [onlovexiahui@qq.com]<br>
 * @since 1.0.0
 */
@Configuration
@Import({ OnlyDispatcherConfig.class, OnlyActionDispatcherPortal.class, OnlyResponseBodyHandleConfig.class })
public class OnlyDispatcherStartConfig {

	private OnlyPathMatchConfigurer pathMatchConfigurer;

	/**
	 * 
	 * {link WebFluxConfigurationSupport} <br>
	 * Date 2020-05-15 12:00:38<br>
	 * 
	 * @param contentTypeResolver
	 * @return
	 * @since 1.0.0
	 */
	@Bean
	public OnlyActionMappingHandlerMapping actionMappingHandlerMapping(
			@Qualifier("webFluxContentTypeResolver") RequestedContentTypeResolver contentTypeResolver) {
		OnlyActionMappingHandlerMapping mapping = createActionMappingHandlerMapping();
		mapping.setOrder(0);
		mapping.setContentTypeResolver(contentTypeResolver);
		// mapping.setCorsConfigurations(getCorsConfigurations());

//		OnlyPathMatchConfigurer configurer = getPathMatchConfigurer();
//		Boolean useTrailingSlashMatch = configurer.isUseTrailingSlashMatch();
//		if (useTrailingSlashMatch != null) {
//			mapping.setUseTrailingSlashMatch(useTrailingSlashMatch);
//		}
//		Boolean useCaseSensitiveMatch = configurer.isUseCaseSensitiveMatch();
//		if (useCaseSensitiveMatch != null) {
//			mapping.setUseCaseSensitiveMatch(useCaseSensitiveMatch);
//		}
//		Map<String, Predicate<Class<?>>> pathPrefixes = configurer.getPathPrefixes();
//		if (pathPrefixes != null) {
//			mapping.setPathPrefixes(pathPrefixes);
//		}

		return mapping;
	}

	/**
	 * Override to plug a sub-class of {@link RequestMappingHandlerMapping}.
	 */
	protected OnlyActionMappingHandlerMapping createActionMappingHandlerMapping() {
		return new OnlyActionMappingHandlerMapping();
	}

	/**
	 * Callback for building the {@link PathMatchConfigurer}. This method is final,
	 * use {@link #configurePathMatching} to customize path matching.
	 */
	protected final OnlyPathMatchConfigurer getPathMatchConfigurer() {
		if (this.pathMatchConfigurer == null) {
			this.pathMatchConfigurer = new OnlyPathMatchConfigurer();
			configurePathMatching(this.pathMatchConfigurer);
		}
		return this.pathMatchConfigurer;
	}

	/**
	 * Override to configure path matching options.
	 */
	public void configurePathMatching(OnlyPathMatchConfigurer configurer) {
	}
}
