package com.onlyxiahui.framework.action.dispatcher.general.handler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.AntPathMatcher;

import com.onlyxiahui.framework.action.dispatcher.ActionContext;
import com.onlyxiahui.framework.action.dispatcher.extend.ActionRequest;
import com.onlyxiahui.framework.action.dispatcher.extend.ArgumentBox;
import com.onlyxiahui.framework.action.dispatcher.extend.ResultHandler;

/**
 * Date 2019-01-12 12:05:12<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class GeneralResultHandler implements ResultHandler {

	List<ResultHandler> handlers;

	public GeneralResultHandler(List<ResultHandler> handlers) {
		this.handlers = handlers;
	}

	AntPathMatcher pm = new AntPathMatcher();
	List<String> excludeList = new ArrayList<>();

	private boolean ignore(String lookupPath) {
		if (null != lookupPath) {
			for (String pattern : excludeList) {
				if (pm.match(pattern, lookupPath)) {
					return true;
				}
			}
		}
		return false;
	}

	public void addExcludePath(String path) {
		excludeList.add(path);
	}

	@Override
	public Object handle(ActionContext actionContext, Object data, ActionRequest request, ArgumentBox argumentBox) {
		String path = request.getPath();
		if (ignore(path)) {
			return data;
		}
		if (null != handlers) {
			for (ResultHandler h : handlers) {
				data = h.handle(actionContext, data, request, argumentBox);
			}
		}
		return data;
	}
}
