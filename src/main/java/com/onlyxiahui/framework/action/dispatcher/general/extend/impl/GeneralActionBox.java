package com.onlyxiahui.framework.action.dispatcher.general.extend.impl;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.onlyxiahui.framework.action.dispatcher.extend.ActionBox;

/**
 * Date 2019-01-12 22:19:52<br>
 * Description
 * 
 * @author XiaHui<br>
 * @since 1.0.0
 */

public class GeneralActionBox implements  ActionBox, ApplicationContextAware {
	private ApplicationContext applicationContext = null;

	@Override
	public Object getAction(Class<?> type) {

		Object o = null;
		if (null != applicationContext) {
			o = applicationContext.getBean(type);
		}
		return o;
	}

	@Override
	public Object getAction(String className) {
		Object o = null;
		if (null != applicationContext) {
			o = applicationContext.getBean(className);
		}
		return o;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
