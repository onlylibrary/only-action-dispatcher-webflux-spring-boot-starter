# only-action-dispatcher-webflux-spring-boot-starter

#### Description
spring webflux的controller请求扩展，使@RequestBody json格式的数据，可以直接按属性路径取对象，如：
```java

	如以下请求格式
	{
	    "body":{
	        "query":{
	            "age":13
	        },
	        "page":{
	            "size":30,
	            "number":2
	        }
	    }
	}
	@ActionMapping("/v1/self/self-order-goods/list")
	public PageResult<List<SelfOrderGoods>> list(
			@Define("body.query") SelfOrderGoodsQuery query,
			@Define("body.page") Page page) {
			
		List<SelfOrderGoods> list = selfOrderGoodsService.queryList(page, query);
		return new PageResult<List<SelfOrderGoods>>(page, list);
	}
	
	
	
	{
	    "body":{
	        "id":10000
	    }
	}
	@ActionMapping("/v1/self/self-order-goods/get-by-id")
	public SelfOrderGoods get(@Validated @Define("body.id") Long id) {
		SelfOrderGoods data = selfOrderGoodsService.getById(id);
		return (data);
	}
	
```